<?php

namespace app\controllers;
use app\models\LoginForm;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use \yii\web\Controller;

class CrudController extends Controller{

    public function __construct($id, Module $module, array $config = [])
    {
        parent::__construct($id, $module, $config);
        Yii::$app->user->loginUrl = ['crud/login'];
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'index', 'logout'],
                'rules' => [
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login'],
                        'allow' => ['true'],
                        'roles' => ['?']
                    ]
                ],
            ]
        ];
    }

    public $layout = 'crud';

    public function actionIndex(){
        return $this->render('index');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->redirect(["crud/index"]);
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->redirect(["crud/index"]);
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout(){
        Yii::$app->user->logout();
        return $this->redirect(["crud/index"]);
    }

    public function actionError(){

    }
}