<?php

use yii\db\Migration;

/**
 * Handles the creation of table `projects`.
 */
class m190122_073240_create_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('projects', [
            'id' => $this->primaryKey(),
            'userId' => $this->integer(),
            'projectName' => $this->string(),
            'cost' => $this->float(),
            'beginDate' => $this->dateTime(),
            'endDate' => $this->dateTime()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('projects');
    }
}
