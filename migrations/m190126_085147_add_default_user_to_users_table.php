<?php

use yii\db\Migration;

/**
 * Class m190126_085147_add_dafault_user_to_users_table
 */
class m190126_085147_add_default_user_to_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('users', ['name' => 'admin',
                                      'login' => 'admin',
                                      'password' => 'admin']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('users', ['login'=> 'admin']);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190126_085147_add_dafault_user_to_users_table cannot be reverted.\n";

        return false;
    }
    */
}
