<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "projects".
 *
 * @property int $id
 * @property int $userId
 * @property string $projectName
 * @property double $cost
 * @property string $beginDate
 * @property string $endDate
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['userId'], 'integer'],
            [['cost'], 'number'],
            [['beginDate', 'endDate'], 'safe'],
            [['projectName'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'userId' => 'User ID',
            'projectName' => 'Project Name',
            'cost' => 'Cost',
            'beginDate' => 'Begin Date',
            'endDate' => 'End Date',
        ];
    }
}
